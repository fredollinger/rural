// From Unix Network Programming Volume 1
// page 6

#ifdef __FreeBSD__
#include <sys/types.h>
#endif

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

//const char *dserver = "128.138.141.172";
const char *dserver = "129.6.15.30";
//const char *dserver = "127.0.0.1";
#define MAXLINE 100

int main(int argc, char *argv[]) {
    int sockfd, n;
    char recvline[MAXLINE];
    struct sockaddr_in servaddr;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Can't create socket. \n");
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(13);

    if (inet_pton(AF_INET, dserver, &servaddr.sin_addr) != 1) {
        printf("inet_pton error \n");
        exit(-1);
    }

    if (connect(sockfd, (struct sockaddr*) &servaddr, sizeof(servaddr)) < 0) {
        printf("connect error \n");
        exit(-1);
    }

    while((n=read(sockfd, recvline, MAXLINE)) > 0) {
        recvline[n] = 0; // null terminate
	if(fputs(recvline, stdout) == EOF) {
	    printf("error \n");
	    exit(-1);
	}
    }

    if (n < 0)
        printf("read errr \n");

    exit(0);
}
