// From Unix Network Programming Volume 1
// page 14

#ifdef __FreeBSD__
#include <sys/types.h>
#endif

#include <arpa/inet.h>
#include <sys/errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/uio.h>
#include <time.h>
#include <unistd.h>

#define MAXLINE 100

#define LISTENQ 10

int main(int argc, char *argv[]) {
    int listenfd, connfd;
    struct sockaddr_in servaddr;
    char buff[MAXLINE];
    time_t ticks;
    int err, b;

    // if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    err = errno;

    if (listenfd < 0) {
        perror("socket() error");
    }

    // TODO: CHECK ERROR HERE

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(13);

    // TODO CHECK ERROR CODES
    int success = bind(listenfd, (struct sockaddr*) &servaddr, sizeof(servaddr));
    err = errno;

    if (0 != success) {
        perror("bind() failed with error \n");
        exit(err);
    }
    else
        printf("bind() success \n");

    success = listen(listenfd, LISTENQ);
    if (0 != success) {
        printf("listen() failed with error \n");
        exit(err);
    }
    else
        printf("listen() success \n");

    while(1) {
        b = sizeof servaddr;
        //      accept(int s, struct sockaddr * restrict addr,
        //      socklen_t * restrict addrlen);
        //
        int b = sizeof(servaddr);
        //connfd = accept(listenfd, (struct sockaddr*) &servaddr, b);
        connfd = accept(listenfd, NULL, NULL);

    if (0 != connfd) {
            perror("accept() fail");
	    exit(connfd);
        }

        printf("writing: [%s] \n", ctime(&ticks));
        snprintf(buff, sizeof(buff), "%.24s\r\a", ctime(&ticks));
        err = write(connfd, buff, strlen(buff));
        if (0 != err) {
            perror("write() fail");
        }
        close(connfd);
    }

    exit(0);
}
