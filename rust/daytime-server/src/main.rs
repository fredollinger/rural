extern crate chrono;

use std::net::{TcpListener, TcpStream};
use std::io::Write;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:13").unwrap();

    fn handle_client(mut stream: TcpStream) {
        use chrono::{Local, DateTime};
        let dt: DateTime<Local> = Local::now();
        let response = format!("{} \n", dt);
        stream.write(response.as_bytes()).expect("Response failed");
    }

    // accept connections and process them serially
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                handle_client(stream);
            }
            Err(e) => {
                println!("Error: [{:?}]", e);
            }
        }
    }
}
