extern crate chrono;

fn main() {
    // use chrono::{DateTime, TimeZone, NaiveDateTime};
    use chrono::{Local, DateTime, TimeZone};
    // let dt = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc);

    let dt: DateTime<Local> = Local::now();
    let dt: DateTime<Local> = Local.timestamp(0, 0);
    println!("Hello, world! {}", dt);
}
