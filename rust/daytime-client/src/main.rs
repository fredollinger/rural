use std::io::prelude::*;
use std::net::TcpStream;
use std::str;

fn main() {

    // TODO: load server from variable instead of magic number below
    //let dserver = String::from("128.138.141.172:13");
    let mut buffer = [0; 1000];

    //let mut stream = TcpStream::connect(("128.138.141.172", 13)).unwrap();
    let mut stream = TcpStream::connect(("127.0.0.1", 13)).unwrap();

    let dt = stream.read(&mut buffer);
    println!("io success {}", dt.is_ok());

    let s = str::from_utf8(&buffer).unwrap();

    println!("result: {}", s);
}
