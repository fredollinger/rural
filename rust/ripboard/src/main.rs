use std::process;
use std::io::{self, Read};

extern crate clipboard;

use clipboard::ClipboardProvider;
use clipboard::ClipboardContext;

fn stdin_imp() -> std::io::Result<()> {
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    handle.read_to_string(&mut buffer)?;
    let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();
    ctx.set_contents(buffer.to_owned()).unwrap();
    print!("[{}]", buffer);
    Ok(())
}

fn stdin() {
    match stdin_imp() {
        Ok(_) => print!(""),
        Err(_) => println!("rcat: stdin failed")
    }
}

fn main() {
    stdin();
    process::exit(0);
/*
    let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();


    // set clipboard contents does not work...
    let the_string = "Hello, world!";
    ctx.set_contents(the_string.to_owned()).unwrap();


// this does print the clipboard properly
//    println!("{}", ctx.get_contents().unwrap());
*/
}
