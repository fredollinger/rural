use std::fs::File;
use std::io::prelude::*;
use std::process;
use std::io::{self, Read};

#[cfg(target_os = "linux")]
fn are_you_on_linux() -> bool {
    return true;
}

#[cfg(not(target_os = "linux"))]
fn are_you_on_linux() -> bool {
    return false;
}

// Print a file with line numbers
fn cat_line_numbers(contents: &str) {
    let lines = contents.lines();

    static mut num: i32 = 1;

    // let mut num = 1;
    for x in lines {
        unsafe {
            println!("     {}\t{}", num, x); // x: i32
            num = num + 1;
        }
    }

    // When we are not on Linux (FreeBSD for example),
    // cat will normally reset the line number for
    // a new file.
    //
    // On Linux, we continue to increment our line number for each new file.
    if ! are_you_on_linux() {
        unsafe {
            num = 1;
        }
    }
}

fn cat_imp(file: &str, print_file_names: bool) -> std::io::Result<()> {
    let mut file = File::open(file)?;
    let mut contents = String::new();
    let res = file.read_to_string(&mut contents);
    // TODO: proper error handling
    // cat: junk: No such file or directory
    // then keep rolling...
    //println!("val [{}] ", print_file_names);
    match res {
            Ok(_) => {
                if print_file_names {
                    cat_line_numbers(contents.as_str());
                }
                else {
                    print!("{}", contents);
                }
                return Ok(());
            },
            Err(_) => return Ok(())
    }
    // Ok(())
}

// NOT DONE
fn help() {
    println!("Usage: cat [OPTION]... [FILE]...");
    println!("Concatenate FILE(s), or standard input, to standard output.");
    println!("Examples:");
    println!("cat        Copy standard input to standard output.");
}

fn stdin_imp() -> std::io::Result<()> {
    loop {
        // FIXME BREAK OUT OF THIS LOOP
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(n) => {
            // TODO Need to exit on ctrl-d
            // The following does NOT work:
            // if input.contains("\u{0004}") {
            //        process::exit(0);
            // }
                print!("{}", input);
            }
            Err(error) => process::exit(0),
        }
    }
    Ok(())
}

fn stdin() {
    match stdin_imp() {
        Ok(_) => print!(""),
        Err(_) => println!("rcat: stdin failed")
    }
}

fn cat(file: &str, print_file_names: bool) {
    match cat_imp(file, print_file_names) {
        Ok(_) => print!(""),
        Err(_) => println!("rcat: {}: No such file or directory", file)
    }
}

fn main() {
    use std::env;

    let mut val = false;

    let args: Vec<_> = env::args().collect();

    if 1 == args.len() {
        stdin();
        process::exit(0);
    }

    let mut files: Vec<_> = Vec::new();

    for arg in &args {
        match arg.as_ref() {
            "-" => {
                stdin();
                process::exit(0);
            }
            "-n" => val=true,
            "--help" => {
                help();
                process::exit(0);
            },
            _ => {
                files.push(arg);
            },
        }
    }

    for file in &files {
        cat(file.as_str(), val);
    }

}
