use std::time::Duration;
use std::env;
use std::io::prelude::*;
use std::net::TcpStream;
use std::str;

fn main() {
    // parse cli argument:
    //let args: Vec<_> = env::args().collect();
    //if args.len() > 1 {
    //    println!("The first argument is {}", args[1]);
    //}

    // TODO: load server from variable instead of magic number below
    //let dserver = String::from("128.138.141.172:13");
    let mut buffer = [0; 1000];

    //172.217.5.206
    //let mut stream = TcpStream::connect(("128.138.141.172", 13)).unwrap();
    let mut stream = TcpStream::connect(("172.217.5.206", 80)).unwrap();
    //let mut stream = TcpStream::connect(("127.0.0.1", 13)).unwrap();

    let five_seconds = Duration::new(5, 0);
    stream.set_read_timeout(None).expect("set_read_timeout call failed");
    //let dt = stream.read(&mut buffer);
    //let mut buf = [0; 10];
    //let len = stream.peek(&mut buf).expect("peek failed");
    //println!("io success {}", dt.is_ok());
    //let s = str::from_utf8(&buffer).unwrap();
    //println!("result: {}", s);
}
